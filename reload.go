package main

import (
	"bytes"
	// "fmt"
	"os"
	"os/exec"
	"text/template"
)

const (
	appleScript = `tell application "{{.Browser}}"
 set windowList to every window
 repeat with aWindow in windowList
	 set tabList to every tab of aWindow
	 repeat with atab in tabList
	 if (URL of atab contains "{{.Keyword}}") then
		 tell atab to {{.ReloadCommand}}
	 end if
	 end repeat
 end repeat
end tell
`
)

type browser struct {
	Browser, Keyword, ReloadCommand string
}

var (
	tmpfiles        map[string]string
	scriptsRendered map[string]bool
	script          *template.Template
)

func init() {
	script = template.New("Browser")
	script, _ = script.Parse(appleScript)
	scriptsRendered = make(map[string]bool)
	tmpfiles = make(map[string]string)
	tmpfiles["Safari"] = os.TempDir() + "devrun-reload-safari"
	tmpfiles["Google Chrome"] = os.TempDir() + "devrun-reload-chrome"
}

func ReloadBrowsers(addr string) error {
	safari := &browser{Browser: "Safari", Keyword: addr, ReloadCommand: "do javascript \"window.location.reload()\""}
	chrome := &browser{Browser: "Google Chrome", Keyword: addr, ReloadCommand: "reload"}

	err := ReloadBrowser(safari)
	if err != nil {
		return err
	}

	err = ReloadBrowser(chrome)
	if err != nil {
		return err
	}
	return nil
}

func ReloadBrowser(browser *browser) error {

	if !scriptsRendered[browser.Browser] {
		f, err := os.Create(tmpfiles[browser.Browser])
		if err != nil {
			return err
		}

		buf := bytes.NewBuffer([]byte(""))
		err = script.Execute(buf, browser)
		if err != nil {
			return err
		}

		_, err = f.Write(buf.Bytes())
		if err != nil {
			panic(err)
		}

		f.Close()
		scriptsRendered[browser.Browser] = true
	}

	script := exec.Command("/usr/bin/osascript", tmpfiles[browser.Browser])

	script.Run()

	// err := script.Run()
	// if err != nil {
	// 	LogInfo(fmt.Sprintf("Problem reloading %s\n", browser.Browser))
	// 	// LogInfo(err.Error())
	// } else {
	// 	// LogInfo(fmt.Sprintf("Reloaded %s\n", browser.Browser))
	// }

	return nil
}
