package main

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"github.com/howeyc/fsnotify"
	"github.com/wsxiaoys/terminal"
	"io"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

// TODO Runner could be smaller and cleaner...
type Runner struct {
	args, buildArgs, path, dir, adirs, addr string
	apaths                                  []string
	reload, test, clear                     bool
	done, cancelReload                      chan bool
	cmd                                     *exec.Cmd
}

func NewRunner(args, buildArgs, dir, adirs, addr string, reload, test, clear bool, done chan bool) (*Runner, error) {

	// Cleanup binary args
	args = strings.Trim(args, " \n")

	// Cleanup go build args
	buildArgs = strings.Trim(buildArgs, " \n")

	// "Hack" needed for os/exec.Cmd function,
	// it is very sensitive about spacing.
	if buildArgs != "" {
		buildArgs += " "
	}

	path, err := filepath.Abs(dir)
	if err != nil {
		return nil, err
	}

	var apaths []string
	if adirs != "" {
		dirs := strings.Split(adirs, ",")
		apaths = make([]string, len(dirs))
		for i, d := range dirs {
			p, err := filepath.Abs(d)
			if err != nil {
				return nil, err
			}

			apaths[i] = p
		}
	}

	runner := &Runner{args, buildArgs, path, dir, adirs, addr, apaths, reload, test, clear, done, make(chan bool), nil}

	// Run binary at boot once, then it gets rerun by the file watcher.
	go runner.run()

	// Watch file changes and run run. Blocks.
	runner.listenToFileChanges()
	return runner, nil
}

func (r *Runner) run() error {

	// Kill any previously running binary (build or run, r.cmd is shared).
	if r.cmd != nil {
		if r.reload {
			r.cancelReload <- true
		}
		r.cmd.Process.Signal(os.Interrupt)
		r.cmd.Wait()
	}

	if r.clear {
		r.cmd = exec.Command("clear")

		r.cmd.Stdout = os.Stdout
		r.cmd.Stderr = os.Stderr
		r.cmd.Stdin = os.Stdin

		// Run will wait for completion of the build.
		err := r.cmd.Run()
		if err != nil {
			panic(err)
		}
	}

	// Move to the project's path.
	err := os.Chdir(r.path)
	if err != nil {
		return err
	}

	var tempFile string
	var vargs []string
	logName := "|"
	path, _ := filepath.Abs(r.path)
	if r.test {
		tempFile = path + "/" + filepath.Base(path) + ".test"
		os.Remove(tempFile)
		vargs = []string{"test", "-c"}
	} else {
		logName = "build"
		// Create temp file *name* (can't use ioutils.TempFile,
		// we just want the name, not the file).
		path, _ := filepath.Abs(r.path)
		tempFile = os.TempDir() + filepath.Base(path) + NextSuffix()
		vargs = strings.Split(("build " + r.buildArgs + "-o " + tempFile), " ")
	}
	r.cmd = exec.Command("go", vargs...)

	// Output go build to current parent process outputs, with prefix and color.
	r.cmd.Stdout = &ColorPrefixPipe{os.Stdout, logName + " ", "g"}
	r.cmd.Stderr = &ColorPrefixPipe{os.Stderr, logName + " ", "r"}

	// Run will wait for completion of the build.
	err = r.cmd.Run()
	if err != nil {
		if errString := strings.Trim(err.Error(), " \n"); errString != "" {
			terminal.Stdout.Color("r").Print(logName + " error: " + errString).Nl().Reset()
		}
		if r.test {
			os.Remove(tempFile)
		}
		// If there was a build error, bail out,
		// wait for a new file change to build new version.
		return nil
	}

	logName = "|"

	// Move to project path. Again. Just to make sure.
	err = os.Chdir(r.path)
	if err != nil {
		return err
	}

	// Run binary with optional args.
	r.cmd = exec.Command(tempFile, strings.Split(r.args, " ")...)

	// Output go run binary to current parent process outputs, with prefix and color.
	r.cmd.Stdout = &ColorPrefixPipe{os.Stderr, logName + " ", "g"}
	r.cmd.Stderr = &ColorPrefixPipe{os.Stderr, logName + " ", "r"}

	// Reload Browser pages when addr is accessible
	if r.reload {
		go r.waitForBrowserReload()
	}

	// Start() starts the binary and returns immediately, you must call Wait
	err = r.cmd.Start()
	if err != nil {
		if r.reload {
			r.cancelReload <- true
		}
		if errString := strings.Trim(err.Error(), " \n"); errString != "" {
			terminal.Stdout.Color("m").Print(logName + "+ error: " + errString).Nl().Reset()
		}
	}

	if r.test {
		r.cmd.Wait()
		os.Remove(tempFile)
	}

	return nil
}

func (r *Runner) listenToFileChanges() error {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}

	// filesState is used to lazily keep track of the content
	// of each file to avoid double compile
	filesState := make(map[string][]byte)
	go func() {
		for {
			select {
			case ev := <-watcher.Event:
				if strings.HasSuffix(ev.Name, ".go") {

					f, openFileErr := os.Open(ev.Name)
					var newmd5 []byte

					if openFileErr == nil {
						hash := md5.New()
						_, err := io.Copy(hash, f)
						LogError(err)

						newmd5 = hash.Sum(nil)
					}
					f.Close()

					oldmd5 := filesState[ev.Name]
					if openFileErr != nil || !bytes.Equal(newmd5, oldmd5) {
						LogInfo(fmt.Sprintf("File changed: %s (%x)\n", ev.Name, newmd5))
						filesState[ev.Name] = newmd5
						r.run()
					}
				}
			case err := <-watcher.Error:
				fmt.Sprintf(fmt.Sprintln("error:", err))
			}
		}
	}()

	// Start watching
	LogInfo("Watching '" + r.path + "'\n")
	err = watcher.Watch(r.path)
	if err != nil {
		return err
	}

	for _, p := range r.apaths {
		LogInfo("Watching additional path: '" + p + "'\n")
		err = watcher.Watch(p)
		if err != nil {
			return err
		}

	}

	return nil
}

func (r *Runner) waitForBrowserReload() {

	// Capture canceled
	canceled := false
	go func() {
		canceled = <-r.cancelReload
	}()

	// Max number of retries, retry for about 20s
	max := 20000
	count := 0
	var err error
	now := time.Now()
	for {
		conn, err := net.Dial("tcp", r.addr)

		if err == nil {
			conn.Close()
			duration := time.Since(now)
			LogInfo(fmt.Sprintf("Time To First Request: ~%dms\n", duration.Nanoseconds()/1000000.0))
			err := ReloadBrowsers(r.addr)
			LogError(err)
			return
		}

		// Limit the number of retries, if it's not happenning, let's not pollute
		count += 1
		if count > max {
			return
		}

		// If the app failed for some reason bailout auto refresh
		if canceled {
			return
		}

		time.Sleep(time.Millisecond)
	}
	LogInfo("Tried to connect for refresh 1000 times, giving up: " + err.Error() + "\n")
}
