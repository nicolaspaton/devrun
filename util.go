package main

import (
	"github.com/wsxiaoys/terminal/color"
	"io"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var colorPipe *ColorPrefixPipe
var errorColorPipe *ColorPrefixPipe

func init() {
	colorPipe = &ColorPrefixPipe{os.Stdout, "| ", "y"}
	errorColorPipe = &ColorPrefixPipe{os.Stdout, "| ", "r"}
}

func LogInfo(in string) {
	colorPipe.Write([]byte(in))
}

func LogError(err error) {
	if err != nil {
		errorColorPipe.Write([]byte(err.Error()))
	}
}

func LogAsError(err string) {
	errorColorPipe.Write([]byte(err))
}

var rand uint32
var randmu sync.Mutex

// Random num as string for temp filename
func NextSuffix() string {
	randmu.Lock()
	r := rand
	if r == 0 {
		r = reseed()
	}
	r = r*1664525 + 1013904223 // constants from Numerical Recipes
	rand = r
	randmu.Unlock()
	return strconv.Itoa(int(1e9 + r%1e9))[1:]
}

func reseed() uint32 {
	return uint32(time.Now().UnixNano() + int64(os.Getpid()))
}

// Color all output with term colors as it goes by
type ColorPrefixPipe struct {
	Writer io.Writer
	Prefix string
	Color  string
}

func (c *ColorPrefixPipe) Write(in []byte) (int, error) {
	parts := strings.Split(strings.Trim(string(in), "\n\r\t "), "\n")
	for _, part := range parts {
		b := []byte(color.Sprint("@" + c.Color + c.Prefix + "@| " + part + "\n"))
		_, err := c.Writer.Write(b)
		if err != nil {
			return 0, err
		}
	}
	return len(in), nil
}
