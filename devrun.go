package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
)

func main() {
	args := flag.String("args", ".", "Args to pass to binary")
	buildArgs := flag.String("buildArgs", "", "Args for go build")
	dir := flag.String("dir", ".", "Directory to watch")
	adirs := flag.String("adirs", "", "Additional Directory to watch")
	addr := flag.String("addr", ":8080", "Address to watch (auto refresh of browsers)")
	reload := flag.Bool("reload", false, "Should auto refresh of browsers")
	test := flag.Bool("test", false, "Do build+tests instead of build+run")
	clear := flag.Bool("clear", true, "Should automatically run `clear` before each run")
	flag.Parse()

	quit := make(chan bool)
	_, err := NewRunner(*args, *buildArgs, *dir, *adirs, *addr, *reload, *test, *clear, quit)
	if err != nil {
		LogError(err)
		os.Exit(1)
	}

	// Catching INTERUPT so ctrl+c goes to the line nicely in zsh... lame.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		fmt.Println("")
		os.Exit(0)
	}()

	// Just block
	<-quit
}
