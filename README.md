Devrun
======

A command line utility to run Go apps during development. Very rough edges, built for personal use, but I figured it could help others.


Install
-------

	go get bitbucket.org/nicolaspaton/devrun


Usage
-----

Automatically rebuilds and reruns if .go files current directory change:

	$ devrun

If it's a command line, it will "go build" then run the executable.

For command lines applications and packages, add -test arg to run tests:

	$ devrun -test

Chrome and Safari reload with -reload arg and change address to watch with -addr:

	$ devrun -reload
	$ devrun -reload -addr=:3000 // defaults to 8080

Calls "clear" on each execution by default, use -clear to deactivate:

	$ devrun -clear=false
